class Roles {
    private Role[] roles;

    private Roles(Role[] roles) {
        this.roles = roles;
    }

    public boolean contains(Role role){
        for (Role role_it :
                this.roles) {
            if(role_it.getName().equals(role.getName())){
                return true;
            }
        }
        return false;
    }

    public static Roles of(Role ... roles){
        return new Roles(roles);
    }

    public static Roles of(String ... roles){
        Role[] roles_obj = new Role[roles.length];
        for (int i=0;i<roles.length;++i) {
            roles_obj[i]=new Role(roles[i]);
        }
        return new Roles(roles_obj);
    }
}
