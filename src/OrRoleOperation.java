import java.util.function.Predicate;

public class OrRoleOperation extends RoleOperation {

    public OrRoleOperation(RoleExpression left, RoleExpression right){
        super(left,right);
    }

    @Override
    public Predicate<Roles> toPredicate() {
        return this.left.toPredicate().or(this.right.toPredicate());
    }

    @Override
    public String toStringPresentation() {
        return "(" + this.left.toStringPresentation() + " | " + this.right.toStringPresentation() +")";
    }
}
