public class Feature {
    private String name;
    private RoleExpression expression;

    public Feature(String name){
        this.name=name;
    }
    public Feature(String name, RoleExpression expression){
        this.name=name;
        this.expression=expression;
    }

    public String getName() {
        return name;
    }

    public RoleExpression getExpression() {
        return expression;
    }

    public void setExpression(RoleExpression expression) {
        this.expression = expression;
    }
}
