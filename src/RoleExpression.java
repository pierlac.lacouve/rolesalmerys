import java.util.function.Predicate;

public interface RoleExpression {
    Predicate<Roles> toPredicate();
    String toStringPresentation();
}
