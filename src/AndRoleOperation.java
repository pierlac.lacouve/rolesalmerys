import java.util.function.Predicate;

public class AndRoleOperation extends RoleOperation {

    public AndRoleOperation(RoleExpression left, RoleExpression right){
        super(left,right);
    }

    @Override
    public Predicate<Roles> toPredicate() {
        return this.left.toPredicate().and(this.right.toPredicate());
    }

    @Override
    public String toStringPresentation() {
        return "(" + this.left.toStringPresentation() + " & " + this.right.toStringPresentation() + ")";
    }
}
