import java.util.function.Predicate;

public abstract class RoleOperation implements RoleExpression{
    protected RoleExpression left;
    protected RoleExpression right;

    protected RoleOperation(RoleExpression left, RoleExpression right){
        this.left=left;
        this.right=right;
    }

}
