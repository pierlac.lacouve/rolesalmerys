public class main {
    public static void main(String[] args) {
        Roles pierre = Roles.of("dev","admin","student");
        Feature feature = new Feature("php",new OrRoleOperation( new AndRoleOperation(new RoleTerm(new Role("dev")),new RoleTerm(new Role("admin"))),new RoleTerm(new Role("student"))));
        System.out.println(feature.getExpression().toStringPresentation());
    }
}
